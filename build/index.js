const version = "0.2.7"

const readline = require("readline")
const fs = require("fs")
const exec_old = require("child_process").exec

const Delay = (ms) => new Promise((resolve, reject) => setTimeout(() => resolve(), ms))

const execP = (command) => {
  return new Promise((resolve, reject) => exec_old(command, (error, stdout, stderr) => {
    if (error) reject({
      error,
      stderr,
      stdout
    })
    else resolve(stdout)
  }))
}

const getUserInput = (question) => {
  let rl = readline.createInterface({
    "input": process.stdin,
    "output": process.stdout
  })
  let qp = new Promise((resolve, reject) => {
    rl.question(question, (answer) => {
      rl.close()
      resolve(answer)
    })
  })
  return [rl, qp]
}

const getSimpleUserInput = (question) => {
  let [rl, qp] = getUserInput(question)
  return qp
}

class Logger {
  constructor (fileDir, fileName, fileExt = ".log") {
    let postfix = 1
    let tFileName = fileName
    while (fs.existsSync(fileDir + tFileName + fileExt)) {
      tFileName = fileName + "_" + postfix
      postfix++
    }
    this.filePath = fileDir + tFileName + fileExt
    this.startedOn = Date.now()
    try {
      fs.writeFileSync(this.filePath, "", {"flag": "w"})
    } catch (e) {
      postfix = 1
      tFileName = fileName
      while (fs.existsSync("X:\\" + tFileName + fileExt)) {
        tFileName = fileName + "_" + postfix
        postfix++
      }
      console.warn("File directory unwritable, trying to write on memory drive: X:\\", false)
      this.filePath = "X:\\" + tFileName + fileExt
    }
  }

  getLogTimestamp() {
    let ts = (Date.now() - this.startedOn) / 1000
    ts = ts.toString()
    let ms = ts.split(".")[1]
    if (isEmpty(ms)) {
      ts += "."
      ms = ""
    }
    for (let i = ms.length; i < 3; i++) ts += "0"
    return ts
  }

  log(data, logType = "INFO") {
    if (typeof data === "object") data = JSON.stringify(data)
    fs.writeFileSync(this.filePath, `[${this.getLogTimestamp()}] ${logType} | ${data}\n`, {"flag": "a"})
  }
}

const alphabet = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "W", "V", "W", "X", "Y", "Z"]
const terminalColors = {
  brighter: "\x1b[1m",
  darker: "\x1b[2m",
  reset: "\x1b[0m",
  white: "\x1b[37m",
  lightWhite: "\x1b[0m\x1b[1m\x1b[37m",
  darkWhite: "\x1b[0m\x1b[2m\x1b[37m",
  red: "\x1b[31m",
  lightRed: "\x1b[0m\x1b[1m\x1b[31m",
  darkRed: "\x1b[0m\x1b[2m\x1b[31m",
  yellow: "\x1b[33m",
  lightYellow: "\x1b[0m\x1b[1m\x1b[33m",
  darkYellow: "\x1b[0m\x1b[2m\x1b[33m",
  cyan: "\x1b[36m",
  lightCyan: "\x1b[0m\x1b[1m\x1b[36m",
  darkCyan: "\x1b[0m\x1b[2m\x1b[36m",
  blue: "\x1b[34m",
  lightBlue: "\x1b[0m\x1b[1m\x1b[34m",
  darkBlue: "\x1b[0m\x1b[2m\x1b[34m",
  green: "\x1b[32m",
  lightGreen: "\x1b[0m\x1b[1m\x1b[32m",
  darkGreen: "\x1b[0m\x1b[2m\x1b[32m",
  purple: "\x1b[35m",
  lightPurple: "\x1b[0m\x1b[1m\x1b[35m",
  darkPurple: "\x1b[0m\x1b[2m\x1b[35m",
  gray: "\x1b[2m\x1b[37m",
  lightGray: "\x1b[0m\x1b[1m\x1b[37m",
  darkGray: "\x1b[0m\x1b[2m\x1b[37m"
}
const orgConsole = {
  "warn": console.warn,
  "error": console.error,
  "debug": console.debug,
  "log": console.log,
  "dir": console.dir
}
console.dir = (input, log = false) => {
  orgConsole.dir(input)
  if (log) logHandler.log(JSON.stringify(input), "INFO")
  return input
}
console.warn = (input, log = true) => {
  if (typeof input === "object") console.dir(input, log)
  else {
    orgConsole.warn(`${terminalColors.yellow}${input || ""}${terminalColors.reset}`)
    if (log && !isEmpty(input)) logHandler.log(input, "WARN")
  }
  return input
}
console.error = (input, log = true) => {
  if (typeof input === "object") console.dir(input, log)
  else {
    orgConsole.error(`${terminalColors.red}${input || ""}${terminalColors.reset}`)
    if (log && !isEmpty(input)) logHandler.log(input, "ERROR")
  }
  return input
}
console.debug = (input, log = false) => {
  if (log && !isEmpty(input)) {
    if (typeof input === "object" && !debugmode) logHandler.log(JSON.stringify(input), "DEBUG")
    else logHandler.log(input, "DEBUG")
  }
  if (!debugmode) return input
  if (typeof input === "object") console.dir(input, log)
  else orgConsole.debug(`${terminalColors.purple}${input || ""}${terminalColors.reset}`)
  return input
}
console.success = (input, log = true) => {
  if (typeof input === "object") console.dir(input, log)
  else {
    orgConsole.log(`${terminalColors.green}${input || ""}${terminalColors.reset}`)
    if (log && !isEmpty(input)) logHandler.log(input, "SUCCESS")
  }
  return input
}
console.log = (input, log = true) => {
  if (typeof input === "object") console.dir(input, log)
  else {
    orgConsole.log(`${input || ""}`)
    if (log && !isEmpty(input)) logHandler.log(input, "INFO")
  }
  return input
}

/**
 * My famous (not really) isEmpty function. It checks if a given variable is empty or not
 * @param {*} variable Anything you want to know if it's empty or not
 * @returns {Boolean} Returns true on empty and false on not empty
 */
function isEmpty(variable) {
  if (variable == null || variable == undefined || (typeof variable === "number") && Number.isNaN(variable)) return true
  if (typeof variable === "string") {
    if (variable === "") return true
  } else if (typeof variable == "object") {
    if (Array.isArray(variable)) {
      if (variable.length == 0) return true
    } else {
      if (Object.keys(variable).length == 0) return true
    }
  }
  return false
}

/**
 * My second famous (not really) isEmptyAny function. It checks if any variable in a given array is empty or not
 * @param {Array} array An array with elements you want to know if any of them are empty
 * @returns {Boolean} Returns true on any of them being empty and false on none of them being empty
 */
function isEmptyAny(array) {
  for (let i = 0; i < array.length; i++)
    if (isEmpty(array[i])) return true
  return false
}

/**
 * @param {String} path The path to a file you want to 'touch'
 * @returns {Promise} Returns a Promise. On resolve it returns true if the file got touched, reject returns an error if any
 * @requires fs
 * @desc Basiclly the Linux `touch` command made with `fs`
 */
const touch = (path) => new Promise((resolve, reject) => {
  fs.open(path, 'a', (err, fnd) => {
    if (err) return reject(err)
    fs.close(fnd, function (err2) {
      if (err2) reject(err2)
      else resolve(true)
    })
  })
})

function getFileName(filePath) {
  let ext = filePath.match(/^.*(?:\\|\/)(.*?(?:\..*)?)$/)
  if (Array.isArray(ext)) return ext[1]
  else return ""
}

async function deleteFile(filePath) {
  try {
    if (!fs.existsSync(filePath)) return true
    switch (process.platform) {
      case 'win32':
      case 'win64':
        return await execP(`del /F "${filePath}"`);
      case 'darwin':
      default:
        return fs.rmSync(filePath);
    }
  } catch (e) {
    let msg = e
    let stacktrace
    if (typeof e === "object") {
      if (!!e.message) {
        msg = e.message
        stacktrace = e.stack
      } else stacktrace = e
    }
    console.error("Error: " + msg, false)
    console.dir(stacktrace, false)
  }
}

/* function playErrorSound() {
  return new Promise(async (resolve, reject) => {
    let sfx = new Audic("beep.wav")
    sfx.volume = 1
    sfx.loop = false
    await sfx.play()
    sfx.addEventListener('ended', () => {
      sfx.destroy();
      resolve()
    });
    Delay(2168).then(() => {
      sfx.destroy()
      resolve()
    })
  })
} */

function isValidSelfDrive(driveLetter) {
  return fs.existsSync(defaults.selfDriveFileCheck.replace("{{selfdrive}}", driveLetter))
}

function isValidWindowsDirectory(winDir) {
  return fs.existsSync(winDir + "\\explorer.exe")
}

async function performDiskPartCommand(commands = []) {
  if (isEmpty(commands)) return null
  fs.writeFileSync(defaults.diskPartScripts.tmp.replace("{{selfdrive}}", data.selfdrive), commands.join("\n"))
  try {
    let output = await execP(`diskpart /s ${defaults.diskPartScripts.tmp.replace("{{selfdrive}}", data.selfdrive)}`)
    console.debug(output, true)
    return output
  } catch (e) {
    console.error("Failed to perform diskpart script.", true)
    if (!isEmpty(e.stderr)) console.error(e.stderr, true)
    console.error(new Error().stack, false)
    return false
  }
}

function getSelfDrive() {
  let tmpSD = null
  let tsd_re = new RegExp(/^([A-Za-z]:)/).exec(process.cwd())
  if (!isEmpty(tsd_re)) tmpSD = tsd_re[1]
  if (isEmpty(tmpSD) || !isValidSelfDrive(tmpSD)) {
    for (let i = 0; i < alphabet.length; i++) {
      const driveLetter = alphabet[i] + ":";
      if (isValidSelfDrive(driveLetter)) {
        console.debug("Found self drive through auto discovery aka brute force.", false)
        return driveLetter
      }
    }
    console.debug("No self drive found.", false)
    return null
  }
  console.debug("Found self drive through 'process.cwd()'.", false)
  return tmpSD
}

function getWindowsDirectory() {
  for (let i = 0; i < alphabet.length; i++) {
    const winDir = alphabet[i] + ":\\Windows";
    if (alphabet[i] === "X") continue
    if (isValidWindowsDirectory(winDir)) {
      console.debug("Found Windows drive through auto discovery aka brute force.", true)
      return winDir
    }
  }
  console.debug("No Windows drive found.", true)
  return null
}

function findWindowsEditionFallback(windowsOS = data.windowsOS, windir = data.windowsDir) {
  const licenseFolder = windir + "\\System32\\Licenses\\neutral\\_Default"
  if (!fs.existsSync(licenseFolder)) return false
  let licenses = fs.readdirSync(licenseFolder)
  return flagToEdition(windowsOS, licenses[0])
}

function getInstalledWindowsArchitecture(winDrive = "C:") {
  let res = (fs.existsSync(`${winDrive}\\Program Files (x86)`)) ? "x64" : "x86"
  console.debug(`Found '${res}' architecture`, true)
  return res
}

function findCorrectProduct(productKeys, productSearchTerm = "Windows") {
  for (let i = 0; i < productKeys.length; i++) {
    const productKey = productKeys[i];
    if (productKey["Product Name"].includes(productSearchTerm)) return productKey
  }
  return null
}

async function getProductData(productSearchTerm = "Windows", windir = data.windowsDir, outputPath = defaults.getProductData.outputPath.replace("{{selfdrive}}", data.selfdrive)) {
  try {
    try {
      console.debug(await execP(defaults.produkey_exec.replace("{{selfdrive}}", data.selfdrive) + ` ${(!devmode) ? `/windir "${windir}"` : ""} /sjson "${outputPath}"`))
    } catch (e2) {
      if (e2.stderr.includes("is not recognized as an internal or external command,")) console.error("ProduKey.exe not found, missing ProduKey lib, unable to extract product data.", false)
      else console.error(e2.stderr, false)
      return null
    }
    const productKeys = JSON.parse(fs.readFileSync(outputPath))
    console.debug("Exported product key data:", false)
    console.debug(productKeys, false)
    if (isEmpty(productKeys)) return null
    const productKey = findCorrectProduct(productKeys, productSearchTerm)
    console.debug("Found correct product key data:", false)
    console.debug(productKey, false)
    let res = {
      "windowsOS": null,
      "windowsEdition": null,
      "productKey": null
    }
    // RegExp product name
    let re_res = new RegExp(/Windows (\d*?) (.*)/).exec(productKey["Product Name"])
    if (!isEmpty(re_res)) {
      res.windowsOS = re_res[1]
      res.windowsEdition = findWindowsEditionFallback(res.windowsOS, windir) || re_res[2]
    }
    // Get product key
    res.productKey = productKey["Product Key"]
    console.debug("Got product data: ", false)
    console.debug(res, false)
    return res
  } catch (e) {
    let msg = e
    let stacktrace
    if (typeof e === "object") {
      if (!!e.message) {
        msg = e.message
        stacktrace = e.stack
      } else stacktrace = e
    }
    console.error("Error: " + msg, false)
    console.dir(stacktrace, false)
    return false
  }
}

const editionToFlag = {
  "10": {
    "Home": "Core",
    "Home N": "CoreN",
    "Home Single Language": "CoreSingleLanguage",
    "Education": "Education",
    "Education N": "EducationN",
    "Pro": "Professional",
    "Pro N": "ProfessionalN",
  },
  "11": {
    "Home": "Core",
    "Home N": "CoreN",
    "Home Single Language": "CoreSingleLanguage",
    "Education": "Education",
    "Education N": "EducationN",
    "Pro": "Professional",
    "Pro N": "ProfessionalN",
  }
}

function flagToEdition(windowsOS, eFlag) {
  if (isEmpty(editionToFlag[windowsOS])) return false
  for (const edition in editionToFlag[windowsOS]) {
    if (Object.hasOwnProperty.call(editionToFlag[windowsOS], edition)) {
      const flag = editionToFlag[windowsOS][edition];
      if (flag === eFlag) return edition
    }
  }
  return null
}

async function getAvailableWindowsEditions(windowsOS = data.windowsOS, windowsArchitecture = data.windowsArchitecture, esdFolder) {
  try {
    if (isEmpty(esdFolder) || !fs.existsSync(esdFolder)) esdFolder = `${defaults.windowsInstallFilesBaseFolder.replace("{{selfdrive}}", data.selfdrive)}\\${windowsOS}\\${windowsArchitecture}`
    if (!fs.existsSync(esdFolder)) {
      console.error("No Windows Images found in: " + esdFolder, false)
      return [false, null]
    }
    let esdFiles
    try {
      console.debug(`Reading folder '${esdFolder}'...`, false)
      console.debug("Stack: ", false)
      console.debug(new Error().stack, false)
      esdFiles = fs.readdirSync(esdFolder)
    } catch (e) {
      e.message = `Failed to read ESD file for fetching available windows editions.\n${e.message}`
      return [e, null]
    }
    let files = []
    if (!isEmpty(esdFiles)) {
      for (let i = 0; i < esdFiles.length; i++) {
        const file = esdFiles[i];
        const filePath = esdFolder + "\\" + file
        if ((!file.toLowerCase().endsWith(".esd") && !file.toLowerCase().endsWith(".wim")) || file.toLowerCase().includes("winre")) continue
        let res
        try {
          res = await execP(`dism /Get-WimInfo /WimFile:${filePath}`)
        } catch (e) {
          console.error(`Failed to perform: 'dism /Get-WimInfo /WimFile:${filePath}'\nMaybe missing Admin rights?`, true)
          if (!isEmpty(e.stderr)) console.error(e.stderr, true)
          console.error(new Error().stack, false)
          continue
        }
        if (isEmpty(res)) continue
        let sr = res.match(/\n(.+)/gm)
        if (isEmpty(sr)) continue
        let inEntry = false
        let entries = []
        let nEntry
        for (let j = 1; j < sr.length; j++) {
          const sr_res = sr[j].replace("\n", "");
          if (sr_res.includes("Index :")) {
            inEntry = true
            nEntry = {}
          } else if (sr_res.includes("Description :")) {
            inEntry = false
            entries.push(nEntry)
          }
          if (!inEntry) continue
          let [key, value] = sr_res.split(" : ")
          nEntry[key] = value
          if (key === "Name") {
            // Windows OS version
            let osVersion = new RegExp(/^Windows (.*?) /).exec(value)
            if (!isEmpty(osVersion)) nEntry.osVersion = osVersion[1]
            // Edition
            let edition = new RegExp(/^Windows .*? (.*)/).exec(value)
            if (!isEmpty(edition)) nEntry.edition = edition[1]
            // Flags
            if (!isEmpty(nEntry.edition) && !isEmpty(nEntry.osVersion)) nEntry.flag = editionToFlag[nEntry.osVersion][nEntry.edition]
          }
        }
        files.push({
          "esdFile": filePath,
          esdFolder,
          entries
        })
      }
    }
    return [files, esdFolder]
  } catch (e) {
    let msg = e
    let stacktrace
    if (typeof e === "object") {
      if (!!e.message) {
        msg = e.message
        stacktrace = e.stack
      } else stacktrace = e
    }
    console.error("Error: " + msg, false)
    console.dir(stacktrace, false)
    return [false, null]
  }
}

class ValidateWindowsEdition {
  constructor() {
    this.initialize = false
  }

  async init(windowsOS, windowsArchitecture) {
    if (this.initialize === true) return this
    if (isEmptyAny([windowsOS, windowsArchitecture])) throw new Error("Missing arguments.")
    let [res, esdFolder] = await getAvailableWindowsEditions(windowsOS, windowsArchitecture)
    this.validEditionsData = res
    if (this.validEditionsData instanceof Error) throw this.validEditionsData
    this.esdFolder = esdFolder
    this.validEditions = ""
    for (let i = 0; i < this.validEditionsData.length; i++) {
      const esdFileData = this.validEditionsData[i];
      if (isEmpty(esdFileData)) continue
      for (let j = 0; j < esdFileData.entries.length; j++) {
        const entry = esdFileData.entries[j];
        if (!isEmpty(this.validEditions)) this.validEditions += ", "
        this.validEditions += `${i}:${entry.edition}`
      }
    }
    this.initialize = true
    console.debug("ValidateWindowsEdition initialization complete!", false)
    console.debug({
      "validEditionsData": this.validEditionsData,
      "validEditions": this.validEditions
    }, false)
    return this
  }

  getValidEditions() {
    return this.validEditions
  }

  getValidEditionsData() {
    return this.validEditionsData
  }

  getEsdFolder() {
    return this.esdFolder
  }

  getEsdFileFromIndex = (index) => {
    console.debug(`Performing getEsdFileFromIndex(${[index].join(", ")})`, false)
    if (isEmpty(this.validEditionsData[index])) {
      console.debug("No valid edition data found in index: " + index, false)
      console.debug(this.validEditionsData, false)
      return null
    }
    return this.validEditionsData[index].esdFile
  }

  getIndexFromESD = (esdFile) => {
    console.debug(`Performing getFirstEsdFileFromEdition(${[esdFile].join(", ")})`, false)
    for (let i = 0; i < this.validEditionsData.length; i++) {
      const ved = this.validEditionsData[i]
      if (ved.esdFile === esdFile) return i
    }
    console.debug("No index found from the ESD file.", false)
    return null
  }

  getFirstEsdFileFromEdition = (edition) => {
    console.debug(`Performing getFirstEsdFileFromEdition(${[edition].join(", ")})`, false)
    for (let i = 0; i < this.validEditionsData.length; i++) {
      const esdFileData = this.validEditionsData[i];
      if (isEmpty(esdFileData)) continue
      for (let j = 0; j < esdFileData.entries.length; j++) {
        const entry = esdFileData.entries[j];
        if (entry.edition === edition) {
          console.debug("Found ESD file from index and edition!", false)
          console.debug([i, esdFileData.esdFile, entry.Index], false)
          return [i, esdFileData.esdFile, entry.Index]
        }
      }
    }
    console.debug("No ESD file found from index and edition.", false)
    return [null, null, null]
  }

  getEsdIndexFromEdition = (index, edition) => {
    console.debug(`Performing getEsdIndexFromEdition(${[index, edition].join(", ")})`, false)
    if (isEmpty(this.validEditionsData[index])) {
      console.debug("No valid index found for: " + index + ":" + edition, false)
      console.debug(this.validEditionsData[index], false)
      return null
    }
    for (let i = 0; i < this.validEditionsData[index].entries.length; i++) {
      const entry = this.validEditionsData[index].entries[i];
      if (entry.edition === edition) {
        console.debug("Found ESD index from edition!", false)
        console.debug(entry.Index)
        return entry.Index
      }
    }
    console.debug("No ESD index found from edition.", false)
    return null
  }

  isValidateWindowsEdition = (index, edition) => {
    if (isEmptyAny([index, edition]) || isEmpty(this.validEditionsData[index])) return false
    return (!isEmpty(this.getEsdIndexFromEdition(index, edition)))
  }
}

class Questions {
  static async selfDrive(def) {
    do {
      if (automode) console.error("Could not find self drive... Please provide the self drive letter manually ie: E:", false)
      let rlp = getUserInput(`Self drive letter${!isEmpty(def) ? ` [${def}]` : ""}: `)[1]
      data.selfdrive = await rlp
      if (isEmpty(data.selfdrive) && !isEmpty(def)) {
        console.debug(`Technician entered nothing (${data.selfdrive}) but default (${def}) is available, setting default...`, false)
        data.selfdrive = def
      } else if (!isEmpty(data.selfdrive)) console.debug(`Technician entered: ${data.selfdrive}`, false)
      else console.debug(`Technician entered nothing (${data.selfdrive}) and no default (${def}) is available`, false)
      console.debug({"isValidSelfdrive": isValidSelfDrive(data.selfdrive), "selfdriveLocation": defaults.selfDriveFileCheck.replace("{{selfdrive}}", data.selfdrive)})
    } while (isEmpty(data.selfdrive) || !isValidSelfDrive(data.selfdrive))
    console.debug(`Valid input.`, false)
    return data.selfdrive
  }

  static async windowsDirectory(def) {
    do {
      if (automode) console.error("Could not find the installed Windows directory... Please provide the installed Windows drive letter manually. ie: C:")
      let rlp = getUserInput(`Installed Windows drive letter${!isEmpty(def) ? ` [${def}]` : ""}: `, false)[1]
      data.windowsDir = await rlp
      if (isEmpty(data.windowsDir) && !isEmpty(def)) {
        console.debug(`Technician entered nothing (${data.windowsDir}) but default (${def}) is available, setting default...`, false)
        data.windowsDir = def
      } else if (!isEmpty(data.windowsDir)) {
        console.debug(`Technician entered: ${data.windowsDir}`, false)
        console.debug("Adding ':\\Windows' to it.", false)
        data.windowsDir += "\\Windows"
      } else console.debug(`Technician entered nothing (${data.windowsDir}) and no default (${def}) is available`, false)
    } while (isEmpty(data.windowsDir) || !isValidWindowsDirectory(data.windowsDir))
    console.debug(`Valid input.`, false)
    return data.windowsDir
  }

  static async productData_windowsOS(def) {
    let validOSVersions = fs.readdirSync(defaults.windowsInstallFilesBaseFolder.replace("{{selfdrive}}", data.selfdrive))
    do {
      if (automode) console.error("Could not find (a valid) Windows version/OS (10 / 11 etc.)... Please provide the OS version manually ie: 10", false)
      console.log(`Valid OS versions are: ${validOSVersions.join(", ")}`, false)
      let rlp = getUserInput(`Windows OS Version${(def) ? ` [${def}]` : ""}: `)[1]
      data.windowsOS = await rlp
      if (isEmpty(data.windowsOS) && !isEmpty(def)) {
        console.debug(`Technician entered nothing (${data.windowsOS}) but default (${def}) is available, setting default...`, false)
        data.windowsOS = def
      } else if (!isEmpty(data.windowsOS)) console.debug(`Technician entered: ${data.windowsOS}`, false)
      else console.debug(`Technician entered nothing (${data.windowsOS}) and no default (${def}) is available`, false)
    } while (isEmpty(data.windowsOS) || validOSVersions.indexOf(data.windowsOS) === -1)
    console.debug(`Valid input.`, false)
    return data.windowsOS
  }

  static async productData_windowsArchitecture(def) {
    let validArchitectures = fs.readdirSync(defaults.windowsInstallFilesBaseFolder.replace("{{selfdrive}}", data.selfdrive) + "\\" + data.windowsOS)
    if (!isEmpty(validArchitectures)) {
      do {
        if (automode) console.error("Could not find installed architecture... Please provide the architecture manually.", false)
        console.log(`Valid architectures are: ${validArchitectures.join(", ")}`, false)
        let rlp = getUserInput(`Architecture${(def) ? ` [${def}]` : ""}: `)[1]
        data.windowsArchitecture = await rlp
        if (isEmpty(data.windowsArchitecture) && !isEmpty(def)) {
          console.debug(`Technician entered nothing (${data.windowsArchitecture}) but default (${def}) is available, setting default...`, false)
          data.windowsArchitecture = def
        } else if (!isEmpty(data.windowsArchitecture)) console.debug(`Technician entered: ${data.windowsArchitecture}`)
        else console.debug(`Technician entered nothing (${data.windowsArchitecture}) and no default (${def}) is available`, false)
      } while (isEmpty(data.windowsArchitecture) || validArchitectures.indexOf(data.windowsArchitecture) === -1)
      console.debug(`Valid input.`, false)
    } else {
      console.error(`No valid architectures images found for Windows ${data.windowsOS}!`, false)
      process.exit(4)
    }
    return data.windowsArchitecture
  }

  static async productData_windowsEdition(def) {
    let vwe = new ValidateWindowsEdition()
    await vwe.init(data.windowsOS, data.windowsArchitecture)
    data.esdFolder = vwe.getEsdFolder()
    if (!isEmpty(vwe.getValidEditions())) {
      do {
        if (automode) console.error("Could not find (a valid) Windows edition (Pro, Home etc.)... Please provide the edition manually.", false)
        let validEditionsData = vwe.getValidEditionsData()
        console.log("", false)
        for (let i = 0; i < validEditionsData.length; i++) console.log(`ESD file #${i} = ${validEditionsData[i].esdFile}`, false)
        console.log(`Valid editions for Windows ${data.windowsOS} ${data.windowsArchitecture} are: ${vwe.getValidEditions()}`, false)
        let rlp = getUserInput(`Edition (case-sensitive)${(def) ? ` [${def}]` : ""}: `)[1]
        let input = await rlp
        let matchRes = input.match(/^(\d+):(.*)/)
        if (!isEmpty(matchRes)) {
          let [_, index, edition] = matchRes
          index = parseInt(index)
          console.debug({
            input,
            index,
            edition
          }, false)
          data.windowsEdition = edition
          data.esdFilePath = vwe.getEsdFileFromIndex(index)
          data.esdIndex = vwe.getEsdIndexFromEdition(index, edition)
        }
        if ((isEmpty(data.windowsEdition) || isEmpty(matchRes)) && !isEmpty(def)) {
          console.debug(`Technician entered nothing (${input}) but default (${def}) is available, setting default...`, false)
          data.windowsEdition = def
          let [vweIndex, vweESD, esdIndex] = vwe.getFirstEsdFileFromEdition(data.windowsEdition)
          data.esdFilePath = vweESD
          data.esdIndex = esdIndex
        } else if (!isEmpty(data.windowsEdition)) {
          console.debug(`Technician entered: ${input}`, false)
          console.debug(`ESD file: ${data.esdFilePath}`, false)
          console.debug(`ESD index: ${data.esdIndex}`, false)
        } else {
          console.debug(`Technician entered nothing (${input}) and no default (${def}) is available`, false)
          console.debug(`ESD file: ${data.esdFilePath}`, false)
          console.debug(`ESD index: ${data.esdIndex}`, false)
        }
      } while (isEmpty(data.windowsEdition) || isEmpty(data.esdFilePath) || isEmpty(data.esdIndex) || !vwe.isValidateWindowsEdition(vwe.getIndexFromESD(data.esdFilePath), data.windowsEdition))
      console.debug(`Valid input.`, false)
    } else {
      console.error(`No valid editions found for Windows ${data.windowsOS} in the Windows Image!`, false)
      process.exit(6)
    }
    return {
      "windowsEdition": data.windowsEdition,
      "esdFolder": data.esdFolder,
      "esdFilePath": data.esdFilePath,
      "esdIndex": data.esdIndex
    }
  }

  static async productData_productKey(def) {
    if (automode) console.error("Could not find a Windows product key... Please provide the product key manually.", false)
    let rlp = getUserInput(`Product key${(def) ? ` [${def}]` : ""}: `)[1]
    data.windowsProductKey = await rlp
    data.windowsProductKey = data.windowsProductKey.trim()
    if (isEmpty(data.windowsProductKey) && !isEmpty(def)) {
      console.debug(`Technician entered nothing (${data.windowsProductKey}) but default (${def}) is available, setting default...`, false)
      data.windowsProductKey = def
    } else if (isEmpty(data.windowsProductKey) || data.windowsProductKey === "g" || data.windowsProductKey === "generic") {
      data.windowsProductKey = genericInstallKeys[data.windowsOS][data.windowsEdition] || ""
      data.genericKeyUsed = true
      console.debug(`Technician entered: ${data.windowsProductKey}`, false)
      console.debug(`Using generic key`, false)
    } else if (data.windowsProductKey === "mb" || data.windowsProductKey === "motherboard") {
      data.windowsProductKey = ""
      console.debug(`Technician entered: ${data.windowsProductKey}`)
      console.debug(`Praying on product key of motherboard.`, false)
    } else if (!isEmpty(data.windowsProductKey)) console.debug(`Technician entered: ${data.windowsProductKey}`, false)
    else console.debug(`Technician entered nothing (${data.windowsProductKey}) and no default (${def}) is available`)
    console.debug(`Valid input.`, false)
    return data.windowsProductKey
  }
}

class AutoMode {
  static async isUSB() {
    try {
      let testFile = `${data.selfdrive}\\test.txt`
      await touch(testFile)
      await deleteFile(testFile)
      data.isUSB = true
      console.debug("Self drive is USB", false)
    } catch (e) {
      data.isUSB = false
      console.debug("Failed to create file on self drive. Must be running from a CD/DVD.", false)
      console.debug("Self drive is CD/DVD", false)
    }
  }

  static selfDrive() {
    return data.selfdrive = getSelfDrive()
  }

  static windowsDirectory() {
    return data.windowsDir = getWindowsDirectory()
  }

  static async productData() {
    return await getProductData()
  }

  static windowsArchitecture() {
    let winDriveLetter = new RegExp(/^([A-Za-z])/).exec(data.windowsDir)[1]
    if (!isEmpty(winDriveLetter)) winDriveLetter += ":"
    return getInstalledWindowsArchitecture(winDriveLetter)
  }
}

// HEY YOU THERE! These are generic installation keys, not activation keys. You can't activate Windows with these.
const genericInstallKeys = {
  "10": {
    "Home": "YTMG3-N6DKC-DKB77-7M9GH-8HVX7",
    "Home N": "4CPRK-NM3K3-X6XXQ-RXX86-WXCHW",
    "Home Single Language": "BT79Q-G7N6G-PGBYW-4YWX6-6F4BT",
    "Education": "YNMGQ-8RYV3-4PGQ3-C8XTP-7CFBY",
    "Education N": "84NGF-MHBT6-FXBX8-QWJK7-DRR8H",
    "Pro": "VK7JG-NPHTM-C97JM-9MPGT-3V66T",
    "Pro N": "2B87N-8KFHP-DKV6R-Y2C8J-PKCKT",
  }
}

async function manualMode() {
  automode = false
  console.log("")
  await Questions.selfDrive(AutoMode.selfDrive())
  // Check if we are running on a USB or CD/DVD
  await AutoMode.isUSB()
  // Finding Windows drive
  console.log("")
  AutoMode.windowsDirectory()
  await Questions.windowsDirectory(data.windowsDir)
  // Getting product data
  let productData = await AutoMode.productData()
  if (productData === false) productData = {}
  console.log("")
  await Questions.productData_windowsOS(productData.windowsOS)
  productData.windowsArchitecture = AutoMode.windowsArchitecture()
  console.log("")
  await Questions.productData_windowsArchitecture(productData.windowsArchitecture)
  console.log("")
  await Questions.productData_windowsEdition(productData.windowsEdition)
  console.log("")
  await Questions.productData_productKey(productData.productKey)
  console.log("")
  if (!data.genericKeyUsed && !isEmpty(productData.productKey) && productData.productKey === data.windowsProductKey && `Windows ${data.windowsOS} ${data.windowsEdition} ${data.windowsArchitecture}` !== `Windows ${productData.windowsOS} ${productData.windowsEdition} ${productData.windowsArchitecture}`) {
    console.error(`Product key found on Windows might NOT be usable since the target installation (Windows ${data.windowsOS} ${data.windowsEdition} ${data.windowsArchitecture}) isn't the same as the current installation (Windows ${productData.windowsOS} ${productData.windowsEdition} ${productData.windowsArchitecture}).`, false)
    console.log("If you are sure press enter, otherwise enter a new Product key.", false)
    await Questions.productData_productKey(data.windowsProductKey)
  }
  if (data.genericKeyUsed) {
    if (!isEmpty(data.windowsProductKey)) console.warn(`No product key given by the technician. Using generic installation key.`, false)
    else console.warn(`No product key given by the technician and no generic key exists in the internal database for this Windows version and edition. Using NO KEY! Praying on the motherboard to activate Windows.`, false)
  } else if (isEmpty(data.windowsProductKey)) console.log(`No product key given by the technician. Praying on the motherboard to active Windows for us.`, false)
  else console.log("Using given product key!", false)
  await Delay(2000)
  console.log("")
  console.warn(`Installing Windows ${data.windowsOS} ${data.windowsEdition} ${data.windowsArchitecture} in 10 seconds...`, true)
  if (!isEmpty(data.windowsProductKey)) {
    console.log(`Using ${(data.genericKeyUsed) ? "generic" : "given"} product key: ${data.windowsProductKey}`, false)
    logHandler.log(`Using ${(data.genericKeyUsed) ? "generic" : "given"} product key.`)
  }
  else console.warn("Using NO PRODUCT KEY!", true)
  console.log("")
  console.log("If this looks fine to you then there is no further input required unless an error occures.", false)
  console.log("If this does NOT look correct press [Enter] to cancel.", false)
  console.log("Keep the USB plugged in until reboot.", false)
  if (devmode) {
    console.debug("")
    if (preInstallCriticalCheck()) console.debug("All data is available!")
    else console.error("Data variable is missing data!", false)
    console.debug("")
    console.debug("Not installing Windows in Developer mode!")
    process.exit(420)
  }
  if (dryrun) {
    console.log("")
    console.log("Dry run complete!", false)
    process.exit(69)
  }
  console.log("")
  let cancel = false
  let [rl, questionPromise] = getUserInput(`Press [Enter] to cancel...`)
  questionPromise.then(() => {
    cancel = true
    console.warn("Technician canceled auto Windows install.", false)
    process.exit(7)
  })
  await Delay(10000)
  rl.close()
  if (!cancel) {
    console.log("")
    return await installWindows()
  }
  console.warn("Technician canceled auto Windows install.", false)
  process.exit(7)
}

async function initializeAutoMode() {
  // Get self drive
  console.log("")
  console.log("Getting self drive...", false)
  AutoMode.selfDrive()
  if (isEmpty(data.selfdrive)) await Questions.selfDrive()
  console.log(`Self drive '${data.selfdrive}' validated!`, true)
  // Check if we are running on a USB or CD/DVD
  await AutoMode.isUSB()
  // Finding Windows drive
  console.log("")
  console.log("Finding Windows drive...", false)
  AutoMode.windowsDirectory()
  if (isEmpty(data.windowsDir)) await Questions.windowsDirectory()
  console.log(`Windows installation found at '${data.windowsDir}'!`, true)
  // Get current (and to be installed) windows version
  console.log("")
  console.log("Getting currently installed Windows data...", false)
  //// Getting Windows data
  let productData = await AutoMode.productData()
  //// Setting Windows OS version
  if ((isEmpty(productData) || productData === false || isEmpty(productData.windowsOS))) await Questions.productData_windowsOS()
  else data.windowsOS = productData.windowsOS
  console.log(`Windows OS Version '${data.windowsOS}' set to be installed!`, true)
  //// Getting Windows architecture
  productData.windowsArchitecture = AutoMode.windowsArchitecture()
  if (isEmpty(productData.windowsArchitecture)) await Questions.productData_windowsArchitecture()
  else data.windowsArchitecture = productData.windowsArchitecture
  console.log(`Windows Architecture '${data.windowsArchitecture}' set!`, true)
  //// Setting Windows Edition
  if ((isEmpty(productData) || productData === false || isEmpty(productData.windowsEdition))) await Questions.productData_windowsEdition()
  else data.windowsEdition = productData.windowsEdition
  ////// Validate Windows data to check if we have an Windows Image for this.
  let vwe = new ValidateWindowsEdition()
  await vwe.init(data.windowsOS, data.windowsArchitecture)
  data.esdFolder = vwe.getEsdFolder()
  let [vweIndex, vweESD, esdIndex] = vwe.getFirstEsdFileFromEdition(data.windowsEdition)
  data.esdFilePath = vweESD
  data.esdIndex = esdIndex
  console.debug("Auto mode after vwe stuff checkpoint:", false)
  console.debug({
    vweIndex,
    vweESD,
    esdIndex,
    data
  }, false)
  if (isEmptyAny([data.esdFilePath, data.esdIndex])) await Questions.productData_windowsEdition()
  console.log(`Windows '${data.windowsEdition}' set to be installed!`, true)
  // Get product code
  data.genericKeyUsed = false
  if ((isEmpty(productData) || productData === false || isEmpty(productData.productKey))) await Questions.productData_productKey()
  else data.windowsProductKey = productData.productKey
  if (!data.genericKeyUsed && !isEmpty(productData.productKey) && `Windows ${data.windowsOS} ${data.windowsEdition} ${data.windowsArchitecture}` !== `Windows ${productData.windowsOS} ${productData.windowsEdition} ${productData.windowsArchitecture}`) {
    console.error(`Product key found on Windows might NOT be usable since the target installation (Windows ${data.windowsOS} ${data.windowsEdition} ${data.windowsArchitecture}) isn't the same as the current installation (Windows ${productData.windowsOS} ${productData.windowsEdition} ${productData.windowsArchitecture}).`, false)
    console.log("If you are sure press enter, otherwise enter a new Product key.", false)
    await Questions.productData_productKey(data.windowsProductKey)
  }
  if (data.genericKeyUsed) {
    if (!isEmpty(data.windowsProductKey)) console.warn(`No product key found and technician didn't provide one. Using generic installation key.`, false)
    else console.warn(`No product key found, technician didn't provide one and no generic key exists in the internal database for this Windows version and edition. Using NO KEY! Praying on the motherboard to activate Windows.`, false)
  } else if (isEmpty(data.windowsProductKey)) console.log(`No product key found and technician didn't provide one. Praying on the motherboard to active Windows for us.`, false)
  else console.log("Product key found and using!", false)
  await Delay(2000)
  // Priting setup details
  console.log("")
  console.warn(`Installing Windows ${data.windowsOS} ${data.windowsEdition} ${data.windowsArchitecture} in 10 seconds...`, true)
  if (!isEmpty(data.windowsProductKey)) {
    console.log(`Using ${(data.genericKeyUsed) ? "generic" : "installed"} product key: ${data.windowsProductKey}`, false)
    logHandler.log(`Using ${(data.genericKeyUsed) ? "generic" : "installed"} product key.`)
  } else console.warn("Using NO PRODUCT KEY!", true)
  console.log("")
  console.log("If this looks fine to you then there is no further input required unless an error occures.", false)
  console.log("If this does NOT look correct press [Enter] to cancel.", false)
  console.log("Keep the USB plugged in until reboot.", false)
  if (devmode || debugmode) {
    console.debug("")
    if (preInstallCriticalCheck()) console.debug("All data is available!", false)
    else console.error("Data variable is missing data!", false)
    if (devmode) {
      console.debug("")
      console.debug("Not installing Windows in Developer mode!", false)
      process.exit(420)
    }
  }
  if (dryrun) {
    console.log("")
    console.log("Dry run complete!", false)
    process.exit(69)
  }
  console.log("")
  let cancel = false
  let [rl, questionPromise] = getUserInput(`Press [Enter] to cancel...`)
  questionPromise.then(() => {
    cancel = true
    console.warn("Technician canceled auto Windows install.", false)
    process.exit(7)
  })
  await Delay(10000)
  rl.close()
  if (!cancel) {
    console.log("")
    return await installWindows()
  }
  console.warn("Technician canceled auto Windows install.", false)
  process.exit(7)
}

function preInstallCriticalCheck() {
  return (!isEmptyAny([data.selfdrive, data.windowsArchitecture, data.windowsEdition, data.windowsOS, data.windowsDir, data.esdFilePath, data.esdIndex, data.esdFolder]))
}

async function getAllDrives(skipLast = true) {
  let diskpart_res = await performDiskPartCommand(["list disk", "exit"])
  if (isEmpty(diskpart_res) || diskpart_res === false) return false
  let diskpart_lines = diskpart_res.match(/\n(.+)/gm)
  if (isEmpty(diskpart_lines)) return false
  let entries = []
  for (let i = 1; i < diskpart_lines.length; i++) {
    const dpl = diskpart_lines[i].replace("\n", "").trim();
    const disk = dpl.match(/Disk (\d+)/)
    if (!isEmpty(disk)) entries.push(disk[1])
  }
  if (skipLast) entries.splice(entries.length - 1, 1)
  return entries
}

async function installWindows() {
  let tmp
  try {
    console.log("")
    console.log("Windows auto install started!", true)
    // Check integerty of data object
    console.log("Checking internal data...", true)
    if (!preInstallCriticalCheck()) {
      console.error("")
      console.error(data, false)
      console.error("Missing critical data.", true)
      process.exit(8)
    }
    // Get all disks
    console.log(`Getting all disks${(data.isUSB) ? " (minus this USB)" : ""}...`, true)
    let disks = await getAllDrives(data.isUSB)
    console.debug(`All disks${(data.isUSB) ? " (minus this USB)" : ""}: `, true)
    console.debug(disks, true)
    // Clean all disks
    console.log(`Cleaning all disks${(data.isUSB) ? " (minus this USB)" : ""}...`, true)
    if (isEmpty(disks)) {
      console.error("")
      console.error("No disks have been found to install Windows on.", true)
      process.exit(3)
    }
    let diskpart_commands = []
    for (let i = 0; i < disks.length; i++) {
      const disk = disks[i];
      diskpart_commands.push(`select disk ${disk}`)
      diskpart_commands.push(`clean`)
    }
    diskpart_commands.push(`exit`)
    console.debug("About to perform the following disk part script:", true)
    console.debug(diskpart_commands, true)
    tmp = await performDiskPartCommand(diskpart_commands)
    if (tmp === false) {
      console.error("")
      console.error("Cleaning all disks failed.", true)
      process.exit(9)
    }
    console.debug(tmp, true)
    // Partition disks
    console.log("Partitioning new Windows disk...", true)
    let diskPartWinPartitioning = [
      "select disk 0",
      "clean",
      "convert gpt",
      "create partition efi size=100",
      `format quick fs=fat32 label="System"`,
      `assign letter="S"`,
      `create partition msr size=16`,
      `create partition primary`,
      `shrink minimum=450`,
      `format quick fs=ntfs label="Windows"`,
      `assign letter="W"`,
      `create partition primary`,
      `format quick fs=ntfs label="Windows RE tools"`,
      `set id=de94bba4-06d1-4d40-a16a-bfd50179d6ac`,
      `assign letter="T"`,
      `exit`
    ]
    console.debug("About to perform the following disk part script:", true)
    console.debug(diskPartWinPartitioning, true)
    tmp = await performDiskPartCommand(diskPartWinPartitioning)
    if (tmp === false) {
      console.error("")
      console.error("Paritioning new Windows drive failed.", true)
      process.exit(9)
    }
    console.debug(tmp, true)
    // Unpack Windows
    console.log("Unpacking Windows...", true)
    console.debug(`Using ESD/WIM file: ${data.esdFilePath}`, false)
    console.debug(`Using ESD/WIM Index: ${data.esdIndex}`, false)
    try {
      console.debug(`Executing: Dism /Apply-Image /ImageFile:${data.esdFilePath} /Index:${data.esdIndex} /ApplyDir:W:\\`, true)
      tmp = await execP(`Dism /Apply-Image /ImageFile:${data.esdFilePath} /Index:${data.esdIndex} /ApplyDir:W:\\`)
      console.debug(tmp, true)
    } catch (e2) {
      console.error(`Failed to perform: Dism /Apply-Image /ImageFile:${data.esdFilePath} /Index:${data.esdIndex} /ApplyDir:W:\\`, true)
      if (!isEmpty(e2.stderr)) console.error(e2.stderr, true)
      console.error(new Error().stack, false)
      console.error("")
      console.error("Windows installation failed.", false)
      process.exit(9)
    }
    // Create system partitions
    console.log("Creating system partitions...", true)
    try {
      console.debug(`Executing: W:\\Windows\\System32\\Bcdboot W:\\Windows`, true)
      tmp = await execP(`W:\\Windows\\System32\\Bcdboot W:\\Windows`)
      console.debug(tmp)
    } catch (e2) {
      console.error(`Failed to perform: W:\\Windows\\System32\\Bcdboot W:\\Windows`, true)
      if (!isEmpty(e2.stderr)) console.error(e2.stderr, true)
      console.error(new Error().stack, false)
      console.error("")
      console.error("Failed to create system partions, installation failed.", true)
      process.exit(9)
    }
    // Install and Register Windows RE (if available)
    if (fs.existsSync(data.esdFolder + "\\winre.wim")) {
      console.log("Installing Windows RE...", true)
      try {
        console.debug(`Executing: mkdir T:\\Recovery\\WindowsRE`, true)
        tmp = await execP(`mkdir T:\\Recovery\\WindowsRE`)
        console.debug(tmp)
      } catch (e2) {
        console.error(`Failed to perform: mkdir T:\\Recovery\\WindowsRE`, true)
        if (!isEmpty(e2.stderr)) console.error(e2.stderr, true)
        console.error(new Error().stack, false)
        console.error("")
        console.error("Creating directory for Windows RE failed, installation failed.", true)
        process.exit(9)
      }
      try {
        console.debug(`Executing: xcopy /H ${data.esdFolder + "\\winre.wim"} T:\\Recovery\\WindowsRE`, true)
        tmp = await execP(`xcopy /H ${data.esdFolder + "\\winre.wim"} T:\\Recovery\\WindowsRE`)
        console.debug(tmp)
      } catch (e2) {
        console.error(`Failed to perform: xcopy /H ${data.esdFolder + "\\winre.wim"} T:\\Recovery\\WindowsRE`, true)
        if (!isEmpty(e2.stderr)) console.error(e2.stderr, true)
        console.error(new Error().stack, false)
        console.error("")
        console.error("Failed copying Windows RE image, installation failed.", true)
        process.exit(9)
      }
      console.log("Registering Windows RE...")
      try {
        console.debug(`Executing: W:\\Windows\\System32\\Reagentc /setreimage /path T:\\Recovery\\WindowsRE /target W:\\Windows`, true)
        tmp = await execP(`W:\\Windows\\System32\\Reagentc /setreimage /path T:\\Recovery\\WindowsRE /target W:\\Windows`)
        console.debug(tmp)
      } catch (e2) {
        console.error(`Failed to perform: W:\\Windows\\System32\\Reagentc /setreimage /path T:\\Recovery\\WindowsRE /target W:\\Windows`, true)
        if (!isEmpty(e2.stderr)) console.error(e2.stderr, true)
        console.error(new Error().stack, false)
        console.error("")
        console.error("Windows RE registration failed, installation failed.", true)
        process.exit(9)
      }
    } else console.debug("No custom Windows RE image found at location: " + data.esdFolder + "\\winre.wim", true)
    // Install Windows product key (if we have one)
    if (!isEmpty(data.windowsProductKey)) {
      console.log("Registering product key...", true)
      try {
        logHandler.log(`Executing: dism /Image:W:\\ /Set-ProductKey:<REDACTED>`, "DEBUG")
        console.debug(`Executing: dism /Image:W:\\ /Set-ProductKey:${data.windowsProductKey}`, false)
        tmp = await execP(`dism /Image:W:\\ /Set-ProductKey:${data.windowsProductKey}`)
        console.debug(tmp)
      } catch (e2) {
        console.error(`Failed to perform: dism /Image:W:\\ /Set-ProductKey:${data.windowsProductKey}`, false)
        logHandler.log(`Failed to perform: dism /Image:W:\\ /Set-ProductKey:<REDACTED>`)
        if (!isEmpty(e2.stderr)) console.error(e2.stderr, true)
        console.error(new Error().stack, false)
        console.error("")
        console.error("Product key installation failed, installation failed.", true)
        process.exit(9)
      }
    } else {
      console.debug("No Windows key found in memory: " + data.windowsProductKey, false)
      logHandler.log("No Windows key found in memory. Not installing a product key ")
    }
    // Clean up (if necessary)
    try {
      console.debug(await deleteFile(defaults.diskPartScripts.tmp), false)
      console.debug("Cleaned up!", false)
    } catch (e2) {
      console.error(`Failed to clean up but that's fine since temp files are living in memory.`, false)
      if (!isEmpty(e2.message)) console.debug(e2.message)
      console.debug(e2.stack || new Error().stack)
    }
    // Jobs done!
    console.log("")
    console.success("Windows has been installed, system will reboot in 10 seconds...", true)
    await Delay(10000)
    process.exit(0)
  } catch (e) {
    let msg = e
    let stacktrace
    if (typeof e === "object") {
      if (!!e.message) {
        msg = e.message
        stacktrace = e.stack
      } else stacktrace = e
    }
    console.error("Error: " + msg, false)
    console.dir(stacktrace, false)
    process.exit(2)
  }
}

var data = {
  "selfdrive": null,
  "windowsDir": null,
  "windowsOS": null,
  "windowsEdition": null,
  "windowsArchitecture": null,
  "windowsProductKey": null,
  "esdFolder": null,
  "esdFilePath": null,
  "esdIndex": null,
  "genericKeyUsed": false,
  "isUSB": true
}

function resetData(preserverSelfDrive = false) {
  let selfdrive = null
  if (preserverSelfDrive) selfdrive = data.selfdrive
  data = {
    "selfdrive": selfdrive,
    "windowsDir": null,
    "windowsOS": null,
    "windowsEdition": null,
    "windowsArchitecture": null,
    "windowsProductKey": null,
    "esdFolder": null,
    "esdFilePath": null,
    "esdIndex": null,
    "genericKeyUsed": false,
    "isUSB": true
  }
  console.debug("Reseted data!", false)
  console.debug(data, false)
}
var defaults = {
  "selfDriveFileCheck": "{{selfdrive}}\\ESD\\IAMNOTME",
  "produkey_exec": "X:\\recovery\\tools\\produkey-x64\\ProduKey.exe",
  "getProductData": {
    "outputPath": "X:\\output.txt"
  },
  "diskPartScripts": {
    "tmp": "X:\\recovery\\tmp.txt"
  },
  "windowsInstallFilesBaseFolder": "{{selfdrive}}\\ESD\\Images",
}
var devmode = false
async function devMode() {
  try {
    console.log("Loading developer mode... Hello, Mr Turner. It's me, Mr Turner! I'M DRIVING WITH MY FEET!", false)
    devmode = true
    defaults = {
      "selfDriveFileCheck": "{{selfdrive}}\\Coding\\WinAutoInstall4PE\\build\\index.js",
      "produkey_exec": "{{selfdrive}}\\Coding\\WinAutoInstall4PE\\tools\\produkey-x64\\ProduKey.exe",
      "getProductData": {
        "outputPath": "{{selfdrive}}\\Coding\\WinAutoInstall4PE\\tools\\produkey-x64\\output.txt"
      },
      "diskPartScripts": {
        "tmp": "{{selfdrive}}\\Coding\\WinAutoInstall4PE\\tmp.txt"
      },
      "windowsInstallFilesBaseFolder": "{{selfdrive}}\\ADK_Project\\AllWindowsImages",
    }
    return true
  } catch (e) {
    let msg = e
    let stacktrace
    if (typeof e === "object") {
      if (!!e.message) {
        msg = e.message
        stacktrace = e.stack
      } else stacktrace = e
    }
    console.error("Error: " + msg, false)
    console.dir(stacktrace, false)
    process.exit(420)
  }
}

var failedUnitTests = {}
async function runUnitTest(func, testName) {
  try {
    return await func()
  } catch (e) {
    if (Array.isArray(testName))
      for (let i = 0; i < testName.length; i++) {
        const tn = testName[i];
        if (i === 0) failedUnitTests[tn] = e
        else failedUnitTests[tn] = `Parent test '${testName[0]}' failed, '${tn}' test relies on parent test.`
      }
    else failedUnitTests[testName] = e
    return false
  }
}

class UnitTests {
  static init(doResetData = true) {
    this.tmp = null
    if (isEmpty(this.unitTestGlobalSuccess)) this.unitTestGlobalSuccess = true
    if (isEmpty(this.unitTestSuccess)) this.unitTestSuccess = true
    automode = false
    unittest = true
    console.debug("Defaults set to: ", false)
    console.debug(defaults, false)
    if (doResetData) resetData(false)
  }

  static async PickTest(test, uterFilePath) {
    switch (test.toLowerCase()) {
      case "auto":
        console.debug(`Picking unit test 'AutoMode' because of input '${test}'.`, false)
        return await this.AutoMode(uterFilePath)
      case "manual":
        console.debug(`Picking unit test 'ManualMode' because of input '${test}'.`, false)
        return await this.ManualMode()
      case "misc":
        console.debug(`Picking unit test 'Misc' because of input '${test}'.`, false)
        await this.AutoMode()
        return await this.Misc()
      case "all":
      default:
        console.debug(`Picking unit test 'All' because of input '${test}'.`, false)
        return await this.All(uterFilePath)
    }
  }

  static async All(uterFilePath) {
    await this.ManualMode()
    await this.AutoMode(uterFilePath)
    await this.Misc()
    return this.unitTestGlobalSuccess
  }

  static async ManualMode() {
    this.init()
    console.log("Running 'question' tests...", false)
    this.unitTestSuccess = true
    let testResults = {}
    testResults["selfDrive"] = await runUnitTest(async () => {
      let res = await Questions.selfDrive()
      console.log("Result: ", false)
      console.dir(res, false)
      console.log("Does the above result look right to you?", false)
      let answer
      do {
        answer = await getSimpleUserInput(`[y\\n]: `)
      } while (answer.trim().toLowerCase() !== "y" && answer.trim().toLowerCase() !== "n")
      if (answer === "n") throw new Error("Technician said this result is incorrect.")
      else return true
    }, "selfDrive")
    testResults["windowsDir"] = await runUnitTest(async () => {
      let res = await Questions.windowsDirectory()
      console.log("Result: ", false)
      console.dir(res, false)
      console.log("Does the above result look right to you?", false)
      let answer
      do {
        answer = await getSimpleUserInput(`[y\\n]: `)
      } while (answer.trim().toLowerCase() !== "y" && answer.trim().toLowerCase() !== "n")
      if (answer === "n") throw new Error("Technician said this result is incorrect.")
      else return true
    }, "windowsDir")
    testResults["windowsOS"] = await runUnitTest(async () => {
      let res = await Questions.productData_windowsOS()
      console.log("Result: ", false)
      console.dir(res, false)
      console.log("Does the above result look right to you?", false)
      let answer
      do {
        answer = await getSimpleUserInput(`[y\\n]: `)
      } while (answer.trim().toLowerCase() !== "y" && answer.trim().toLowerCase() !== "n")
      if (answer === "n") throw new Error("Technician said this result is incorrect.")
      else return true
    }, "windowsOS")
    testResults["windowsArchitecture"] = await runUnitTest(async () => {
      let res = await Questions.productData_windowsArchitecture()
      console.log("Result: ", false)
      console.dir(res, false)
      console.log("Does the above result look right to you?", false)
      let answer
      do {
        answer = await getSimpleUserInput(`[y\\n]: `)
      } while (answer.trim().toLowerCase() !== "y" && answer.trim().toLowerCase() !== "n")
      if (answer === "n") throw new Error("Technician said this result is incorrect.")
      else return true
    }, "windowsArchitecture")
    this.tmp = await runUnitTest(async () => {
      let res = await Questions.productData_windowsEdition()
      console.log("Result: ", false)
      console.dir(res, false)
      console.log("Does the above result look right to you?", false)
      let answer
      do {
        answer = await getSimpleUserInput(`[y\\n]: `)
      } while (answer.trim().toLowerCase() !== "y" && answer.trim().toLowerCase() !== "n")
      if (answer === "n") throw new Error("Technician said this result is incorrect.")
      else return true
    }, ["windowsEdition", "esdFolder", "esdFilePath", "esdIndex"])
    testResults["windowsEdition"] = this.tmp
    testResults["esdFolder"] = this.tmp
    testResults["esdFilePath"] = this.tmp
    testResults["esdIndex"] = this.tmp
    testResults["windowsProductKey"] = await runUnitTest(async () => {
      let res = await Questions.productData_productKey()
      console.log("Result: ", false)
      console.dir(res, false)
      console.log("Does the above result look right to you?", false)
      let answer
      do {
        answer = await getSimpleUserInput(`[y\\n]: `)
      } while (answer.trim().toLowerCase() !== "y" && answer.trim().toLowerCase() !== "n")
      if (answer === "n") throw new Error("Technician said this result is incorrect.")
      else return true
    }, "windowsProductKey")
    for (const test in testResults) {
      if (Object.hasOwnProperty.call(testResults, test)) {
        const res = testResults[test];
        if (res === true) console.success(`'${test}' test: Success`, false)
        else {
          this.unitTestSuccess = false
          console.error(`'${test}' test: Failed, error: ${failedUnitTests[test].message || failedUnitTests[test].stderr || failedUnitTests[test]}`, false)
        }
      }
    }
    console.log("")
    if (this.unitTestSuccess === true) console.success(`'questions' test success!`, false)
    else {
      console.error(`'questions' test failed!`, false)
      this.unitTestGlobalSuccess = false
    }
    console.log("")
    return this.unitTestSuccess
  }

  static async AutoMode(uterFilePath) {
    this.init()
    console.log("Running 'AutoMode' tests", false)
    this.unitTestSuccess = true
    let selfDrive = getSelfDrive()
    let predefinedUnitTestExpectedResultPaths = [
      uterFilePath,
      `${process.cwd()}\\uter.json`,
      `X:\\recovery\\uter.json`,
      `${selfDrive}\\ESD\\uter.json`,
    ]
    console.debug("predefinedUnitTestExpectedResultPaths: ", false)
    console.debug(predefinedUnitTestExpectedResultPaths, false)
    this.expectedAutoData = null
    for (let i = 0; i < predefinedUnitTestExpectedResultPaths.length; i++) {
      const filePath = predefinedUnitTestExpectedResultPaths[i];
      if (isEmpty(filePath)) continue
      if (fs.existsSync(filePath)) {
        console.debug(`Found uter.json at: ${filePath}`)
        this.expectedAutoData = JSON.parse(fs.readFileSync(filePath))
      }
    }
    if (isEmpty(this.expectedAutoData)) {
      console.error("No uter.json found anywhere. Place it at one of the following locations: ", false)
      if (isEmpty(predefinedUnitTestExpectedResultPaths[0])) console.error(`Or give up your own path via: --uter-file="<file_path>"`, false)
      for (let i = 0; i < predefinedUnitTestExpectedResultPaths.length; i++) {
        const filePath = predefinedUnitTestExpectedResultPaths[i];
        if (isEmpty(filePath)) continue
        console.log(filePath, false)
      }
      process.exit(10)
    }
    await runUnitTest(async () => AutoMode.selfDrive(), "selfdrive")
    await runUnitTest(async () => await AutoMode.isUSB(), "isUSB")
    await runUnitTest(async () => AutoMode.windowsDirectory(), "windowsDir")
    let productData = await runUnitTest(async () => {
      let res = await AutoMode.productData()
      console.debug("productData returned with: ", false)
      console.debug(res, false)
      return res
    }, "productData")
    await runUnitTest(async () => data.windowsArchitecture = AutoMode.windowsArchitecture(), "windowsArchitecture")
    if (!isEmpty(productData)) {
      data.windowsOS = productData.windowsOS
      data.windowsEdition = productData.windowsEdition
      let vwe = new ValidateWindowsEdition()
      await vwe.init(data.windowsOS, data.windowsArchitecture)
      data.esdFolder = vwe.getEsdFolder()
      let [vweIndex, vweESD, esdIndex] = vwe.getFirstEsdFileFromEdition(data.windowsEdition)
      data.esdFilePath = vweESD
      data.esdIndex = esdIndex
      data.windowsProductKey = productData.productKey
    } else {
      console.debug("productData returned with: ", false)
      console.debug(productData, false)
      let failedTestText = "Test 'productData' failed and this test depends on it."
      failedUnitTests["windowsOS"] = failedTestText
      failedUnitTests["windowsEdition"] = failedTestText
      failedUnitTests["esdFilePath"] = failedTestText
      failedUnitTests["esdIndex"] = failedTestText
      failedUnitTests["windowsProductKey"] = failedTestText
    }
    data.genericKeyUsed = undefined // 'windowsProductKey' would be empty
    for (const key in this.expectedAutoData) {
      const result = this.expectedAutoData[key];
      switch (key) {
        case "productData":
          if (isEmpty(productData)) {
            console.error(`'${key}' test: Failed, error: Test 'productData' failed and this test depends on it.`, false)
            this.unitTestSuccess = false
            break
          }
          if (!isEmpty(failedUnitTests[key])) {
            console.error(`'${key}' test: Failed, error: ${failedUnitTests[key].message || failedUnitTests[key].stderr || failedUnitTests[key]}`, false)
            this.unitTestSuccess = false
          }
          for (const key2 in result) {
            if (Object.hasOwnProperty.call(result, key2)) {
              const productDataExpected = result[key2];
              if (productData[key2] === productDataExpected) console.success(`'${key}->${key2}' test: Success`, false)
              else console.warn(`'${key}' test: Returned something unexpected. Expected (${typeof productDataExpected})'${productDataExpected}' | Received (${typeof productData[key2]})'${productData[key2]}'`, false)
            }
          }
          break;
        case "dataObj":
          break;
        default:
          if (data[key] === result) console.success(`'${key}' test: Success`, false)
          else if (isEmpty(failedUnitTests[key])) console.warn(`'${key}' test: Returned something unexpected. Expected (${typeof result})'${result}' | Received (${typeof data[key]})'${data[key]}'`, false)
          else {
            console.error(`'${key}' test: Failed, error: ${failedUnitTests[key].message || failedUnitTests[key].stderr || failedUnitTests[key]}`, false)
            this.unitTestSuccess = false
          }
          break;
      }
    }
    console.log("")
    if (this.unitTestSuccess === true) console.success(`'AutoMode' test success!`, false)
    else {
      console.error(`'AutoMode' test failed!`, false)
      this.unitTestGlobalSuccess = false
    }
    console.log("")
    return this.unitTestSuccess
  }

  static async Misc() {
    if (this.unitTestGlobalSuccess === true) {
      console.log("Running 'Misc' tests", false)
      try {
        if (isValidSelfDrive(data.selfdrive)) console.success(`'isValidSelfDrive' test success`, false)
        else {
          console.error(`'isValidSelfDrive' test failed`, false)
          console.debug(isValidSelfDrive(data.selfdrive), false)
          this.unitTestSuccess = false
        }
        if (isValidWindowsDirectory(data.windowsDir)) console.success(`'isValidWindowsDirectory' test success`, false)
        else {
          console.error(`'isValidWindowsDirectory' test failed`, false)
          console.debug(isValidWindowsDirectory(data.windowsDir), false)
          this.unitTestSuccess = false
        }
        if (await performDiskPartCommand(["list disk", "exit"]) !== false) console.success(`'performDiskPartCommand' test success`, false)
        else {
          console.error(`'performDiskPartCommand' test failed`, false)
          console.debug(await performDiskPartCommand(["list disk", "exit"]), false)
          this.unitTestSuccess = false
        }
        if (getSelfDrive() === data.selfdrive) console.success(`'getSelfDrive' test success`, false)
        else {
          console.error(`'getSelfDrive' test failed`)
          console.debug(getSelfDrive())
          this.unitTestSuccess = false
        }
        if (getWindowsDirectory() === data.windowsDir) console.success(`'getWindowsDirectory' test success`, false)
        else {
          console.error(`'getWindowsDirectory' test failed`, false)
          console.debug(getWindowsDirectory(), false)
          this.unitTestSuccess = false
        }
        if (findWindowsEditionFallback(data.windowsOS, data.windowsDir) === data.windowsEdition) console.success(`'findWindowsEditionFallback' test success`, false)
        else {
          console.error(`'findWindowsEditionFallback' test failed`, false)
          console.debug(findWindowsEditionFallback(data.windowsOS, data.windowsDir), false)
          this.unitTestSuccess = false
        }
        if (getInstalledWindowsArchitecture(new RegExp(/^([A-Za-z])/).exec(data.windowsDir)[1] + ":") === data.windowsArchitecture) console.success(`'getInstalledWindowsArchitecture' test success`, false)
        else {
          console.error(`'getInstalledWindowsArchitecture' test failed`, false)
          console.debug(getInstalledWindowsArchitecture(new RegExp(/^([A-Za-z])/).exec(data.windowsDir)[1] + ":"), false)
          this.unitTestSuccess = false
        }
        this.tmp = [{
            'Product Name': 'Internet Explorer',
            'Product Key': 'VK7JG-NPHTM-C97JM-9MPGT-3V66T',
            'Installation Folder ': '',
          },
          {
            'Product Name': 'Windows 10 Pro',
            'Product Key': 'VK7JG-NPHTM-C97JM-9MPGT-3V66T',
            'Installation Folder ': 'C:\\WINDOWS',
          }
        ]
        if (findCorrectProduct(this.tmp)["Product Key"] === "VK7JG-NPHTM-C97JM-9MPGT-3V66T") console.success(`'findCorrectProduct' test success`, false)
        else {
          console.error(`'findCorrectProduct' test failed`, false)
          console.debug(findCorrectProduct(this.tmp), false)
          this.unitTestSuccess = false
        }
        this.tmp = await getProductData()
        for (const key in this.tmp) {
          if (Object.hasOwnProperty.call(this.tmp, key)) {
            const res = this.tmp[key];
            if (res !== this.expectedAutoData.productData[key]) {
              console.error(`'getProductData' test failed at '${key}' with '${res}' !== '${this.expectedAutoData.productData[key]}'`, false)
              this.unitTestSuccess = false
              this.tmp = false
              break
            }
          }
        }
        if (this.tmp !== false) console.success(`'getProductData' test success`, false)
        else console.debug(await getProductData(), false)
        if (flagToEdition("10", "Professional") === "Pro") console.success(`'flagToEdition' test success`, false)
        else {
          console.error(`'flagToEdition' test failed`, false)
          console.debug(flagToEdition("10", "Professional"), false)
          this.unitTestSuccess = false
        }
        this.tmp = await getAvailableWindowsEditions(data.windowsOS, data.windowsArchitecture, data.esdFolder)
        console.debug(this.tmp, false)
        let found = false
        for (let i = 0; i < this.tmp[0].length; i++) {
          const esd = this.tmp[0][i];
          if (esd.esdFile === data.esdFilePath && esd.esdFolder === data.esdFolder) {
            for (let j = 0; j < esd.entries.length; j++) {
              const entry = esd.entries[j];
              if (entry.osVersion === data.windowsOS && entry.edition === data.windowsEdition) {
                found = true
                break
              }
            }
            if (found === true) break
          }
        }
        if (found === true) console.success(`'getAvailableWindowsEditions' test success`, false)
        else {
          console.error(`'getAvailableWindowsEditions' test failed`, false)
          console.debug(this.tmp, false)
          this.unitTestSuccess = false
        }
      } catch (e2) {
        console.error("'Misc functions' test unexpectedly failed.", false)
        console.error(e2, false)
        this.unitTestSuccess = false
      }
      // Finished all misc functions tests
      console.log("")
      if (this.unitTestSuccess === true) console.success(`'Misc functions' test success!`, false)
      else {
        console.error(`'Misc functions' test failed!`, false)
        this.unitTestGlobalSuccess = false
      }
      console.log("")
      return this.unitTestSuccess
    } else {
      console.warn("Can't perform 'Misc functions' test since previous tests failed.", false)
      return false
    }
  }
}

async function testCode() {
  try {
    let t = new Logger("D:\\", "test")
    await Delay(9)
    console.log(t.getLogTimestamp())
    await deleteFile("D:\\test.log")
    process.exit(0)
  } catch (e) {
    let msg = e
    let stacktrace
    if (typeof e === "object") {
      if (!!e.message) {
        msg = e.message
        stacktrace = e.stack
      } else stacktrace = e
    }
    console.error("Error: " + msg, false)
    console.dir(stacktrace, false)
    process.exit(69)
  }
}

function printVersion(displayCredits = true) {
  console.log(`WinAutoInstall4PE v${version} ${(displayCredits) ? "- Developed by: Andrew de Jong" : ""}`, true)
  console.log("")
}

function help() {
  if (!isEmpty(process.argv[0].match(/node(?:\.exe)?$/))) console.log(`node ${process.argv[1]} [OPTIONS]`, false)
  else console.log(`${getFileName(process.argv[0])} [OPTIONS]`, false)
  console.log("")
  console.log("Options: ", false)
  console.log(`  --auto-mode                          Skip the 10 seconds delay and immediately start in auto mode.`, false)
  console.log(`  --manual-mode                        Skip the 10 seconds delay and immediately start in manual mode.`, false)
  console.log("")
  console.log(`  --debug                              Enables debug mode.`, false)
  console.log(`  --dev-mode                           Enables developer mode. (this probably won't work on your device unless you change the paths in devMode())`, false)
  console.log(`  --dry-run                            Doesn't install Windows but runs all the recon functions.`, false)
  console.log(`  --unit-test=[all,manual,auto,misc]   Runs all the unit tests.`, false)
  console.log(`  --uter-file="<filepath>"             Custom file path of where to find the expected test results in JSON. (escape special characters)`, false)
  console.log(`  --help                               Displays this message.`, false)
  console.log(`  --version                            Displays version.`, false)
}

var logHandler = {
  "log": () => {}
}
var unittest = false
var automode = true
var debugmode = false
var dryrun = false
async function main() {
  try {
    if (process.argv.indexOf("--dev-mode") !== -1) await devMode()
    if (!isEmpty(process.argv.join(" ").match(/--log(?:=)(?:"(.*?)")/)) && process.argv.indexOf("--version") === -1 && process.argv.indexOf("--help") === -1) {
      console.log("Enabling log system...", false)
      let fileDir = process.argv.join(" ").match(/--log(?:=)(?:"(.*?)")/)
      if (!isEmpty(fileDir)) {
        fileDir = fileDir[1].replace("{{selfdrive}}", getSelfDrive())
        let now = Date.now()
        let fileName = await getSimpleUserInput(`File name for log? [${now}]:`)
        logHandler = new Logger(fileDir, (isEmpty(fileName) ? now : fileName))
      } else {
        console.error("No log directory has been given. Can't start logging.", false)
        process.exit(11)
      }
    }
    printVersion()
    if (process.argv.indexOf("--version") !== -1) process.exit(0)
    if (process.argv.indexOf("--help") !== -1) {
      help()
      process.exit(0)
    }
    if (process.argv.indexOf("--debug") !== -1) {
      console.debug("Debug mode enabled, prepare to die!", false)
      debugmode = true
    }
    console.debug("Process arguments: " + process.argv.join(" "), false)
    if (process.argv.indexOf("--dry-run") !== -1) dryrun = true
    if (!isEmpty(process.argv.join(" ").match(/--unit-test(?:=)?(?:([A-Za-z]*))?/))) {
      console.log(`Loading unit tests...`, false)
      let test = process.argv.join(" ").match(/--unit-test(?:=)?(?:([A-Za-z]*))?/)
      let fileParam = process.argv.join(" ").match(/--uter-file(?:=)?(?:"(.*?)")?/)
      if (!isEmpty(test) && !isEmpty(test[1])) test = test[1]
      else test = "all"
      if (!isEmpty(fileParam)) fileParam = fileParam[1]
      let res = await UnitTests.PickTest(test, fileParam)
      if (res === true && test === "all") console.success("Everything works as intended!", false)
      else if (res === false && test === "all") console.error("I'm broken.", false)
      process.exit(0)
    }
    if (process.argv.indexOf("--test") !== -1) await testCode()
    if (process.argv.indexOf("--auto-mode") !== -1) initializeAutoMode()
    else if (process.argv.indexOf("--manual-mode") !== -1) manualMode()
    else {
      let [rl, questionPromise] = getUserInput(`Running auto mode in 10 seconds, for manual mode press [Enter]...`)
      questionPromise.then(async (manualMode) => {
        automode = false
        console.log("Running manual mode! Good luck, captian!", false)
        manualMode()
        process.exit(0)
      })
      Delay(10000).then(() => {
        if (automode === true) {
          rl.close()
          console.log("")
          console.log("Executing auto mode...", false)
          initializeAutoMode()
        }
      })
    }
  } catch (e) {
    let msg = e
    let stacktrace
    if (typeof e === "object") {
      if (!!e.message) {
        msg = e.message
        stacktrace = e.stack
      } else stacktrace = e
    }
    console.error("Error: " + msg, false)
    console.dir(stacktrace, false)
    process.exit(1)
  }
}
main()