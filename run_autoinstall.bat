@echo off
title WinAutoInstall4PE
goto run

:run
X:\recovery\winautoinstall4pe.exe
if "%errorlevel%" EQU "0" (
  exit 0
) else (
  echo Press [Enter] to try again or CTRL+C to enter shell...
  pause > nul
  goto run
)