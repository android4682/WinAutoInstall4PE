# WinAutoInstall4PE

My own personal tool to install Windows without user interaction. And it's fast too!

## Requirements

- WinRE on a stick (with this program preloaded see [Setup](#setup) for more info)
- Some ESD/WIM files with Windows installations
- Node.JS (made on v16.13.0)
- pkg (a npm package that is globally installed)
- A copy of [ProduKey](https://www.nirsoft.net/utils/product_cd_key_viewer.html) (or the one from this project)

## Setup

A quick guide on how to set this all up.

### 1. Get your hands on winautoinstall4pe.exe

Get yourself a copy of an .exe version this project.
Either by downloading from [this GitLab](https://gitlab.com/android4682/WinAutoInstall4PE/-/releases) or [building it yourself](#build).

### 2. Preparing Windows PE

Get a Windows PE ISO and unpack `boot.wim` located in `sources\`. *If you don't have a Windows PE ISO, you can create one following this [Microsoft guide](https://docs.microsoft.com/en-us/windows-hardware/manufacture/desktop/winpe-create-usb-bootable-drive?view=windows-11#create-a-winpe-iso-dvd-or-cd).*

Then mount `boot.wim` via `DISM /Mount-image /imagefile:<path_to_boot.wim> /MountDir:<target_mount_directory>` or use a third party tool like [NTLite](https://www.ntlite.com/).

Once mounted create a folder called `recovery` in the mounted folder and drop the `winautoinstall4pe.exe` in there.
If you want the tool to auto restart when an error occurs you can also copy over `run_autoinstall.bat` to the `recovery` folder. *Don't forget to add this to the `Windows\System32\startnet.cmd` file if you want this to be auto run on boot up.*

Also copy over the `tools` folder of this project over to the `recovery` folder.

You should now have it looking like this:

```
C:\recovery
  - \winautoinstall4pe.exe
  - \run_autoinstall.bat (optional)
  - \tools
    - \produkey-x64
      - \ProduKey.chm
      - \ProduKey.exe
      - \readme.txt
```

Unmount `boot.wim` and save the changes (make sure you close the folder window of the mounted directory to prevent corruption) by running `Dism /Unmount-image /MountDir:<target_mount_directory> /Commit` or using `NTLite`.

Edit the Windows PE ISO and replace `sources\boot.wim` with your version of `boot.wim`.

### 3. Setup ESD/WIM files

Get some ESD/WIM files from somewhere and set it up the following way.

1. Create a folder in the ISO called `ESD`.
2. Create a file in this `ESD` folder called `IAMNOTME` (no extention). *This file is necessary for the program to find the drive with the ESD/WIM files.*
3. Create a folder in `ESD` called `Images`.
  - The folder structure should be the following: \[Windows version (ie: 10)\] -> \[Architecture (ie: x64)\] -> *\.esd/\*.wim files
  - **(Optional)** You can also drop your `winre.wim` (it has to be called `winre` all lower case) in the last folder of your folder structure, like: \[Windows version (ie: 10)\] -> \[Architecture (ie: x64)\] -> winre.wim
4. Create necessary folder structures
4. Move your ESD/WIM files to the folder structure


### 4. Finishing the ISO

Save the ISO and put it on a DVD or USB (via something like [Rufus](https://rufus.ie/)).

### 5. Boot from USB/DVD

Now on the target computer you can boot from USB/DVD and run `X:\recovery\winautoinstall4pe.exe`.

And that is all! Hf gl gg!

## Build

To build this program from source code:

1. Install Node.JS (preferably v16.13.0)
2. Download this project
3. Install `pkg` globally by running `npm install -g pkg`
4. Run `npm run build`
5. ???
6. Profit!!1

You can find the application in `dist`.

*[Go back to setup](#2-preparing-windows-pe)*

## Credits

Head Developer: [Andrew de Jong](https://gitlab.com/android4682)